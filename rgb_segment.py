import cv2
import helper_functions as fn

# TODO: Add calibration method???
file_name = 'IMG_0131.JPG'
image = cv2.imread('Greenhouse/' + file_name)
resized_image = fn.image_resize(image, scale=10)
print('Input image shape', image.shape)

greyImage = fn.final_image(resized_image, base_mode='grey', mask_mode='Green')
blackImage = fn.final_image(resized_image, base_mode='black', mask_mode='white')
print("Before grayscaling", blackImage.shape)
greenOnBlack = fn.final_image(resized_image, base_mode='black', mask_mode='green')
# area = input("Enter the area for contour: ")
contours = fn.get_contours(blackImage)
#Calculating centroid coordinate of contour

for number, contour in enumerate(contours):
    M = cv2.moments(contour)
    if M['m00'] != 0:
        cx = int(M['m10']/M['m00'])
        cy = int(M['m01']/M['m00'])
    else:
        cx, cy = 0, 0

    xs_grn_val = fn.cal_excs_green(greenOnBlack, cx, cy)
    print(f" {number} ----- Cx is {cx} and Cy is {cy}")
    print("Excess green", xs_grn_val)
    cv2.putText(greyImage, str(xs_grn_val), (cx, cy), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 0)
    # cv2.putText(greenOnBlack, ".", (cx, cy), cv2.FONT_HERSHEY_SIMPLEX, 0.25, (255, 255, 255), 0)


cv2.drawContours(greyImage, contours, -1, (0, 0, 255), 1)
cv2.drawContours(greenOnBlack, contours, -1, (0, 0, 255), 1)
cv2.imshow('Final Image', greyImage)
# cv2.imshow('Final Image on Black', blackImage)

cv2.imshow('Another Image on Black', greenOnBlack)
# cv2.imwrite('Greenhouse/' + file_name + 'onBlack.JPG', blackImage)
# cv2.imwrite('Greenhouse/' + file_name + 'onGrey.JPG', greyImage)
cv2.waitKey(0)
cv2.destroyAllWindows()
