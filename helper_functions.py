import numpy as np
import cv2

"""Resize the image based on the scale by which we want to decrease"""


def image_resize(image, scale):
    width = image.shape[1] // scale
    height = image.shape[0] // scale
    resized_image = cv2.resize(image, (width, height))

    return resized_image


"""Returns the pixel coodinates where a specific criteria is met"""


def pixel_coordinates(image):
    result = np.logical_and(image[:, :, 1] > image[:, :, 2],
                            image[:, :, 2] > image[:, :, 0])
    result = np.where(result == True)

    return result


""" Make a 3 channel base image either greyscale or black """


def base_3_channel(image, mode):  # creates a base 3 channel image can be grey or black
    if mode.lower() == 'grey' or mode.lower() == 'gray':
        base_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        base_image = np.stack((base_image,) * 3, axis=-1)
        # return base_image
    else:
        base_image = np.random.randint(0, high=1, size=(image.shape[0], image.shape[1], 3),
                                       dtype='uint8')

    return base_image


""" Final process for creating output"""


def final_image(image, base_mode, mask_mode):
    # Resize the image
    # resized_image = image_resize(image, scale=10)
    # cv2.imshow('Resized Image', resized_image)
    # Get pixel coordinates where green value is in dominance
    result = pixel_coordinates(image)
    rowIndices = result[0]  # Rows where pixel has required attribute.
    colIndices = result[1]  # Columns where pixel has required attribute.
    # Create base image for adding pixel values
    base_image = base_3_channel(image, mode=base_mode)

    """ The base image is populated """
    if mask_mode.lower() == 'green':
        for row, col in zip(rowIndices, colIndices):
            base_image[int(row), int(col), :] = [image[row, col, 0], image[row, col, 1],
                                                 image[row, col, 2]]

        return base_image

    else:
        for row, col in zip(rowIndices, colIndices):
            base_image[int(row), int(col), :] = [255, 255, 255]

        return base_image


def get_contours(image, area=25):
    """Process to remove the dot contours, by filtering contours with area less than 20."""
    image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    img, cntrs, hierarchy = cv2.findContours(image, cv2.REDUCE_SUM, cv2.CHAIN_APPROX_SIMPLE)
    contours = []
    for contour in cntrs:
        if cv2.contourArea(contour) > area:
            contours.append(contour)

    return contours


def cal_excs_green(image, cx, cy):
    B, G, R = image[cy, cx, 0], image[cy, cx, 1], image[cy, cx, 2]
    if B == 0 or G == 0 or R == 0:
        Nred, Ngreen, Nblue = 0, 0, 0
    else:
        Nred = int(R) / (int(R)+int(G)+int(B))
        Ngreen = int(G) / (int(R)+int(G)+int(B))
        Nblue = int(B) / (int(R)+int(G)+int(B))

    ExGr = round((2*Ngreen)-Nred-Nblue, 3)
    return ExGr

#
"""
ExGr= 2*Ngreen – Nred – Nblue (the value should be between -1 and 2) 
Nred = (Red)/(R+G+B)
Ngreen = (Green)/(R+G+B)
Nblue = (blue)/(R+G+B)
 """


